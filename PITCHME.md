---?image=img/KTU_logo_EN.jpg
---?image=img/KTU_logo_EN.jpg&opacity=10
@title[COVER]
P160M127 Master’s Final Degree Project

Kęstutis Daugėla
***
### Customer review classification 
### in Lithuanian language 
### for e-commerce business 

---
@title[Motivation]
### Motivation for this use case

- Less resources
- Fast responses
- Effective marketing decisions
- Changing customer opinion

---
@title[Objectives]
### Objectives of the project

<ol>
<li class="fragment">Methodology for sentiment analysis in Lithuanian language</li>
<li class="fragment">Machine learning algorithms</li>
<li class="fragment">Microservice for ELT process</li>
<li class="fragment">Microservice for customer review classification</li>
</ol>

---
@title[Data]
##### Results of similar use cases

Author, year | Language | Classes | Best model | Accuracy
------------------------- | --- | --- | --- | --- | 
(Socher, 2013) | EN | 5 | RNTN | **0.807**
(Tang, 2015) | EN | 5 | LSTM-GRNN | **0.676**
(Zhou, 2016) | EN | 2 | BLSTM-2DCNN | **0.895**
(Vyas, 2018) | EN | 2 | SVM | **0.791**


+++
@title[Data]
##### Results of similar use cases
Author, year | Language | Classes | Best model | Accuracy
------------------------- | --- | --- | --- | --- | 
(Dzikienė, 2013) | LT | 3 | NB | **0.679**
(Špats, 2016) | LV | 3 | NB | **0.620**

---?image=/img/microservices.png&size=70%
+++?image=/img/r_microservice.png&size=50%

---
@title[Dataset]
### www.evertink.lt data

- Over 40 e-commerce companies
- Over 16000 reviews

+++?image=/img/reviewcount.png&size=70%
+++?image=/img/reviewrate.png&size=70%
+++?image=/img/reviewtime.png&size=70%

+++
#### Most common issues
- Bad quality goods
- Broken goods during transportation
- Delivery timing issues
- Impolite consultants
- Different specifications

---
@title[Architecture]
### High level application architecture

+++?image=/img/System_overview.png&size=80%
+++?image=/img/tables_all_zones.png&size=80%

---
@title[Architecture]
### Machine learning models

* Decision Tree
* Random Forests
* Support Vector Machine
* Deep learning models

+++?image=/img/TF_models.png&size=70%

---
@title[Results]
### Data samples 

* Stems
* Lemmas
* Latin encoding
* Latin encoding without stopwords

negative/neutral/positive (1500/1000/1500)
---
@title[Data]
##### Best classification models

Dataset | Model | Accuracy | Recall
--- | --- | --- | ---
Latin w/o sw | SVM | **0.767** | 0.737 
Latin | Embedded NN | **0.766** | 0.729 
Latin | SVM | **0.760** | 0.730 
Stems | Embedded NN | **0.760** | 0.721 

+++
@title[Data]
##### 5-Folds cross validation

Dataset | Model | Mean accuracy 
--- | --- | --- | ---
Latin w/o sw | SVM | **0.747** 
Latin | Embedded NN | **0.758** 
Latin | SVM | **0.758** 
Stems | Embedded NN | **0.751**

---
@title[DEMO]
### Microservices

* Analytical application
	* TensorFlow Model
	* Input transformation
* Data flow application

+++?image=/img/analytical_application.png&size=70%

+++
@title[Tensorflow model]
<iframe width="800" height="600" src="http://127.0.0.1:8089/" frameborder="0" allowFullScreen="true"></iframe>

+++
@title[Analytical application]
<iframe width="800" height="600" src="http://127.0.0.1:8090/__swagger__/" frameborder="0" allowFullScreen="true"></iframe>

+++
@title[Data flow application]
<iframe width="800" height="600" src="http://127.0.0.1:8098/__swagger__/" frameborder="0" allowFullScreen="true"></iframe>


---
@title[Conclusions]
### Conclusions

<ol>
<li class="fragment">Reusable methodology for sentiment analysis was created</li>
<li class="fragment">Best methods - DNN and SVM</li>
<li class="fragment">Additional text pre-processing didn't improve the result</li>
<li class="fragment">Additional training data and hyperparameter tuning is needed</li>
<li class="fragment">Unconventional approach for Microservices development was successfully implemented</li>
</ol>

---
@title[Questions]
### Your reviews are welcome!

<iframe width="800" height="300" src="http://127.0.0.1:8090/__swagger__/" frameborder="0" allowFullScreen="true"></iframe>

bitbucket.org/KestutisD/pr00m132_final_project 




